<?php
/**
 * Copyright © MagePal LLC. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Rawveg\NSG\Block\Checkout;

use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;

class Success extends Template
{
    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $checkoutSession;

    /**
     * @var \MagePal\CheckoutSuccessMiscScript\Helper\Data
     */
    protected $helper;

    /**
     * @var \Magento\Sales\Api\OrderRepositoryInterface
     */
    protected $orderRepository;

    protected $order;

    /**
     * @param Context $context
     * @param \MagePal\CheckoutSuccessMiscScript\Helper\Data $helper
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \Magento\Sales\Api\OrderRepositoryInterface $orderRepository
     * @param array $data
     */
    public function __construct(
        Context $context,
        \Rawveg\NSG\Helper\Data $helper,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
        array $data = []
    ) {
        $this->helper = $helper;
        $this->checkoutSession = $checkoutSession;
        $this->orderRepository = $orderRepository;
        parent::__construct($context, $data);
    }

    protected function _toHtml()
    {
        if (!$this->helper->isEnabled()) {
            return '';
        }

        return parent::_toHtml();
    }

    /**
     * @return \Magento\Sales\Api\Data\OrderInterface
     */
    protected function getOrder()
    {
        if (!$this->order) {
            $this->order = $this->orderRepository->get($this->checkoutSession->getLastOrderId());
        }

        return $this->order;
    }

    /**
     * [isEnabled description]
     * @return boolean [description]
     */
    protected function isEnabled()
    {
      return $this->helper->isEnabled();
    }

    /**
     * [getHash description]
     * @return string [description]
     */
    protected function getHash()
    {
      return $this->helper->getHash();
    }

    /**
     * [getIncrementId description]
     * @return int [description]
     */
    protected function getIncrementId()
    {
      return $this->getOrder()->getIncrementId();
    }

    /**
     * [getBaseSubtotal description]
     * @return float [description]
     */
    protected function getBaseSubtotal()
    {
      return $this->getOrder()->getBaseSubtotal();
    }

    /**
     * [getOrderCurrencyCode description]
     * @return string [description]
     */
    protected function getOrderCurrencyCode()
    {
      return $this->getOrder()->getOrderCurrencyCode();
    }

    /**
     * [getCustomerEmail description]
     * @return string [description]
     */
    protected function getCustomerEmail()
    {
      return $this->getOrder()->getCustomerEmail();
    }

    /**
     * @param $order
     * @return $this
     */
    public function setOrder($order)
    {
        $this->order = $order;
        return $this;
    }

}
