<?php
namespace Rawveg\NSG\Block;

use \Magento\Framework\View\Element\Template;

class Seals extends Template
{

    protected static $_isCheckoutSuccess = null;

    /**
     * @var \Rawveg\NSG\Helper\Data
     */
    protected $nortonShoppingGuaranteeHelper;

    /**
     * @var \Magento\Sales\Model\OrderFactory
     */
    protected $salesOrderFactory;

    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $checkoutSession;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Rawveg\NSG\Helper\Data $nortonShoppingGuaranteeHelper,
        \Magento\Sales\Model\OrderFactory $salesOrderFactory,
        \Magento\Checkout\Model\Session $checkoutSession,
        array $data = []
    ) {
        $this->nortonShoppingGuaranteeHelper = $nortonShoppingGuaranteeHelper;
        $this->salesOrderFactory = $salesOrderFactory;
        $this->checkoutSession = $checkoutSession;
        parent::__construct(
            $context,
            $data
        );
    }


    /**
     * Determine whether the merchant hash is set.
     *
     * @return boolean
     */
    public function hasHash()
    {
         return $this->nortonShoppingGuaranteeHelper->hasHash();
    }

    /**
     * Get the merchant hash.
     *
     * @return string|boolean
     */
    public function getHash()
    {
         return $this->nortonShoppingGuaranteeHelper->getHash();
    }

    /**
     * Determine whether a store number is available.
     *
     * @return boolean
     */
    public function hasStoreNumber()
    {
        return $this->nortonShoppingGuaranteeHelper->hasStoreNumber();
    }

    /**
     * Get the current order model.
     *
     * @return \Magento\Sales\Model\Order
     */
    public function getOrder()
    {
        if (!$this->getData('order')) {
            $this->setData(
                'order',
                $this->salesOrderFactory->create()->load(
                    (int) $this->checkoutSession->getLastOrderId()
                )
            );
        }

        return $this->getData('order');
    }

    /**
     * Get the store number.
     *
     * @return string|boolean
     */
    public function getStoreNumber()
    {
        return $this->nortonShoppingGuaranteeHelper->getStoreNumber();
    }

    /**
     * Determine whether the current page is for checkout success.
     *
     * @return boolean
     */
    public function isCheckoutSuccess()
    {
        if (is_null(self::$_isCheckoutSuccess)) {
            self::$_isCheckoutSuccess = false;

            $blocks = array('checkout.success', 'checkout_success');
            $routes = array('checkout_onepage_success', 'checkout_multishipping_success');

            if (in_array(Mage::app()->getFrontController()->getAction()->getFullActionName(), $routes)) {
                self::$_isCheckoutSuccess = true;
            } else {
                foreach ($blocks as $block) {
                    if ($this->getLayout()->getBlock($block) !== false) {
                        self::$_isCheckoutSuccess = true;
                        break;
                    }
                }
            }
        }

        return self::$_isCheckoutSuccess;
    }

    /**
     * Determine whether the feature is enabled.
     *
     * @return boolean
     */
    public function isEnabled()
    {
        return $this->nortonShoppingGuaranteeHelper->isEnabled();
    }

}
