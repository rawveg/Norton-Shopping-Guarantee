<?php
namespace Rawveg\NSG\Helper;


class Data extends \Magento\Framework\App\Helper\AbstractHelper
{

    /**
     * Get the merchant hash.
     *
     * @return string|boolean
     */
    public function getHash()
    {
        return ($this->hasHash()) ? $this->getConfig('rawveg_nsg/general/nsg_hash') : false;
    }

    /**
     * Get the store number.
     *
     * @return string|boolean
     */
    public function getStoreNumber()
    {
        return ($this->hasStoreNumber()) ? $this->getConfig('rawveg_nsg/general/nsg_shop') : false;
    }

    /**
     * Determine whether the merchant hash is set.
     *
     * @return boolean
     */
    public function hasHash()
    {
      return (!is_null($this->getConfig('rawveg_nsg/general/nsg_hash'))) ? true : false;
    }

    /**
     * Determine whether a store number is available.
     *
     * @return boolean
     */
    public function hasStoreNumber()
    {
        return (!is_null($this->getConfig('rawveg_nsg/general/nsg_shop'))) ? true : false;
    }

    /**
     * Determine whether the feature is enabled.
     *
     * @return boolean
     */
    public function isEnabled()
    {
        return ($this->getConfig('rawveg_nsg/general/enable')  == 1) ? true : false;
    }

    /**
     * Gets config value from core_config_data
     * @param  string $config_path Path to Configuration value
     * @return mixed              Result
     */
    public function getConfig($config_path)
    {
      return $this->scopeConfig->getValue($config_path, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

}
