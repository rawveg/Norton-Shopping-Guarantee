<?php
/**
 * @author Tim Green <tim@totallywicked.co.uk>
 */

use Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(ComponentRegistrar::MODULE, 'rawveg_nsg', __DIR__);
